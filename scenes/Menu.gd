extends Control

@onready var score: Label = get_tree().get_nodes_in_group("score")[0]
@onready var eta: Timer = get_tree().get_nodes_in_group("eta")[0]
@onready var spawner: Node2D = get_tree().get_nodes_in_group("spawner")[0]

func _input(event):
	if not event.is_pressed() and (
		event is InputEventKey or
		event is InputEventMouseButton or
		event is InputEventScreenTouch or
		event is InputEventJoypadButton):
		set_process(true)
		set_physics_process(true)
		score.text = '0'
		spawner.is_active = true
		set_process_input(false)
		eta.start(15)
		hide()
