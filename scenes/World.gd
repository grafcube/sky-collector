extends Node2D

@onready var menu =	$Menu
@onready var spawner = $Spawner
@onready var buffer = $Buffer

func _ready():
	set_process(false)
	set_physics_process(false)
	spawner.is_active = false
	buffer.start()

func _on_timeout():
	get_tree().call_group("items", "queue_free")
	set_process(false)
	set_physics_process(false)
	spawner.is_active = false
	menu.show()
	buffer.start()

func _on_buffer_timeout():
	menu.set_process_input(true)
