extends Area2D

@export var speed = 200

@onready var score: Label = get_tree().get_nodes_in_group("score")[0]

func _process(delta):
	position.y += speed * delta

func _on_body_entered(body):
	if body.is_in_group("player"):
		var points = score.text.to_int() + 1
		score.text = str(points)
		queue_free()

func _on_screen_exited():
	queue_free()
