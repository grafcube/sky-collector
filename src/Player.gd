extends CharacterBody2D

@export var speed = 300.0

@onready var sprite = $Sprite

func _physics_process(_delta):
	var direction = Input.get_axis("player_left", "player_right")
	if direction:
		velocity.x = direction * speed
	else:
		velocity.x = move_toward(velocity.x, 0, speed)

	move_and_slide()

func _process(_delta):
	if Input.is_action_pressed("player_left"):
		sprite.play("left")
	elif Input.is_action_pressed("player_right"):
		sprite.play("right")
	else:
		sprite.stop()
