extends Node2D

@onready var collect = $Collect

var is_active: bool = false

func _on_timer_timeout():
	if is_active:
		var item = collect.create_instance()
		item.position.y = -1
		item.position.x = 20 + (randi() % 700)
		add_child(item)
